package main

import (  "fmt"
         "github.com/mailgun/mailgun-go"
         "log"
         "os"
       )

var (
       	mg mailgun.Mailgun
       )

func main() {
    mg = mailgun.NewMailgun(
        mustGetenv("MAILGUN_DOMAIN_NAME"),
        mustGetenv("MAILGUN_API_KEY"),
        mustGetenv("MAILGUN_PUBLIC_KEY"))
    _,bounces,err := mg.GetBounces(99999,0)
    if err != nil {
      log.Fatal(err)
    }
    for _, bounce := range bounces {
        fmt.Printf("%s\n", bounce.Address)
    }
}

func mustGetenv(k string) string {
	v := os.Getenv(k)
	if v == "" {
		log.Fatal("%s environment variable not set.", k)
	}
	return v
}
